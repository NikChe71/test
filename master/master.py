#!/usr/bin/env python3.6

import sys
import utils_ip
import argparse
import subprocess
from time import asctime
from flask import Flask, request, jsonify

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('--ip', help='host of the application')
arg_parser.add_argument('--port', help='port of the application', default = 5000)
arg_parser.add_argument('--directory', help='Shared directory')
args = arg_parser.parse_args()

app = Flask(__name__)

@app.route('/', methods = ['GET', 'POST'])
def log_monitor_ip():
    if request.method == 'POST':
        try:
            # Getting data from monitor that posts its own string
            monitor_ip = request.form['monitor_ip']
            # Logging all data
            print('MONITOR\t|\tWORKER\t\t|\tTIMESTAMP\n{}\t|\t{}'.format(monitor_ip, asctime()), file=sys.stderr)
            return(jsonify({"Status":200}))
        except Exception as e:
            return(jsonify({"Status":'Wrong parameter.{}'.format(e)}))
    else:
        return(jsonify({"Status":"Post some data"}))

if __name__ == '__main__':
    ip = utils_ip.get_ip()
    port = args.port
    print(' ---[info]---\n ip: {}\n port: {}\n ---[info]---'.format(ip, port)) # Test

    # Writing own IP to master.txt in shared folder
    with open('./{}/master.txt'.format(args.directory), 'a') as master:
        master.write(ip + '\n')
    app.run(host = ip, port = port, debug = False)