import subprocess

def get_ip():
    p = subprocess.Popen(['hostname', '-i'], stdout=subprocess.PIPE)
    ip = str(p.communicate()[0])[2:-3]

    return ip