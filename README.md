# test

## Arguments
* D - shared directory
* N - scale parameter of monitor service (placed in docker-compose.yml)
* M - scale parameter of worker service (placed in docker-compose.yml)
* K - TCP port (placed in docker-compose.yml)

## Networking
All services are linked together:

* **monitor** links on **master**
 
* **worker** links on **monitor**

Each worker service connects to each monitor service by IP.

## How to run

```bash
D='directory_name' N=3 M=2 K=5000 docker-compose up --build master monitor worker
```

## Proof
![image](https://bitbucket.org/NikChe71/test/raw/b0767f0da2d1be3357b1d3db3464d6b19582160c/image.jpg)