#!/usr/bin/env python3.6

import sys
import argparse
import requests
from time import sleep
sys.path.append('/app/master')
from master import utils_ip
from flask import Flask, request, jsonify


arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('--ip', help='host of the application')
arg_parser.add_argument('--port', help='port of the application', default = 5000)
arg_parser.add_argument('--directory', help='Shared directory')
args = arg_parser.parse_args()

app = Flask(__name__)

# Method for sending data to monitor
def send_ip(own_ip, own_port):
    with open('./{}/monitors.txt'.format(args.directory), 'r') as monitors:
        for monitor in monitors.readlines():
            # Waiting
            sleep(2)
            # Address of monitor service
            address = 'http://{}:{}/'.format(monitor[0:-1], own_port)
            # Data that we will send to monitor service
            data = {
                "worker_ip": str(own_ip)
            }
            # Post request
            r = requests.post(address, data=data)

if __name__ == '__main__':

    ip = utils_ip.get_ip()
    port = args.port
    print(' ---[info]---\n ip: {}\n port: {}\n ---[info]---'.format(ip, port)) # Test
    # POST request to all monitor services
    send_ip(ip, port)