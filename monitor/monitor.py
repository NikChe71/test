#!/usr/bin/env python3.6

import sys
import argparse
import requests
sys.path.append('/app/master')
from master import utils_ip
from flask import Flask, request, jsonify


arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('--ip', help='host of the application')
arg_parser.add_argument('--port', help='port of the application', default = 5000)
arg_parser.add_argument('--directory', help='Shared directory')
args = arg_parser.parse_args()

app = Flask(__name__)

# Method for sending data to master
def send_ip(master_ip_, master_port_, own_ip_, worker_ip_):
    # Concatenating two IP's in one string
    own_plus_worker = '{}\t|\t{}'.format(own_ip_, worker_ip_)
    data= {
        'monitor_ip': own_plus_worker
        }
    # Sending post request to master
    r = requests.post('http://{}:{}'.format(master_ip_, master_port_), data=data, timeout=1)

# Method for getting data from worker and posting to master
@app.route('/', methods = ['GET', 'POST'])
def log_monitor_ip():
    if request.method == 'POST':
        # Get own ip
        own_ip = utils_ip.get_ip()
        # Reading master ip from master.txt
        with open('./{}/master.txt'.format(args.directory), 'r') as master:
            line = master.readlines()
            master_ip = line[-1][0:-1]
        # Reading form data that comes from worker
        worker_ip = request.form['worker_ip']
        # Sending concatenated IP's to master
        send_ip(master_ip, args.port, own_ip, worker_ip)
        return(jsonify({'Status':'200', 'master_ip': master_ip, 'worker_ip': worker_ip, 'tcp_port': args.port}))
    else:
        return(jsonify({'Status':'Try to use POST method'})) 

if __name__ == '__main__':

    ip = utils_ip.get_ip()
    port = args.port
    print(' ---[info]---\n ip: {}\n port: {}\n ---[info]---'.format(ip, port)) # Test
    
    # Write own IP to monitors.txt
    with open('./{}/monitors.txt'.format(args.directory), 'a') as monitor:
        monitor.write(ip + '\n')
    app.run(host = ip, port = port, debug = False)